# `advfload` - advanced forceload library

A Minetest forceloading library. Internally uses transient Minetest API forceloads.

## Features:

- Named area forceloads.
- Forceload limit is neutralized.
- Exposes information about forceloads to other mods.
- Persistent forceloads (as well as transient).

## Usage

    advfload.start(name, blockpos[, persistent])
    -- Starts forceload `name` at block position `blockpos` (Minetest vector).
    -- If a forceload with the same name exists, it will be replaced.
    -- Persists the forceload after server restart if `persistent` is truthy.
    -- Returns true if successful, false and an error string otherwise.
    
    advfload.start(name, blockpos1, blockpos2[, persistent])
    -- Starts forceload `name` in block area `blockpos1`-`blockpos2`.
    
    advfload.stop(name)
    -- Stops forceload `name`
    -- Returns true if successful, false and an error string otherwise.
    
    advfload.query(name)
    -- Returns information about forceload `name`. Returns nil if non-existent.
    -- ex. advfload.query("test") -> {pos1={x=5,y=-1,z=1}, pos2={x=6,y=0,z=3}, persistent=true}
    
    advfload.query(blockpos[, blockpos2])
    -- Returns all forceloads at `blockpos` (or block area `blockpos`-`blockpos2`)
    -- {
    --   -- started with advfload.start
    --   test = {pos1={x=5,y=0,z=-1}, pos2={x=6,y=1,z=-1}, persistent=true},
    --
    --   -- started with minetest.forceload_block
    --   -- name format: "(minetest)$transient$pos" where:
    --   -- $transient: "P" if persistent, "T" if transient
    --   -- $pos: minetest.pos_to_string(blockpos)
    --   ["(minetest)T(5,1,-1)"]={
    --     pos1={x=5,y=0,z=-1}, pos2={x=5,y=0,z=-1},
    --     persistent=false, count=3
    --   }
    -- }
    
    advfload.query()
    -- Returns all forceloads.
    
    minetest.forceload_block(pos[, transient])
    -- Increments (if exists) or starts (if not) a "(minetest)..." forceload at the block the node `pos` is in.
    -- Persists the forceload if `transient` is falsy.
    -- Transient and persistent forceloads are separate.
    -- Returns true if successful, false otherwise.
    
    minetest.forceload_free_block(pos[, transient])
    -- Decrements or stops a "(minetest)..." forceload at the block the node `pos` is in.
    -- Returns nil if no forceload was found, true if decremented/stopped successfully, false otherwise.
